<?php
include ("head.php");
require_once( "admin/common.inc.php" );
require_once( "admin/config.php" );
require_once( "admin/NewsItem.class.php" );

$start = isset( $_GET["start"] ) ? (int)$_GET["start"] : 0;
$order = isset( $_GET["order"] ) ? preg_replace( "/[^ a-zA-Z]/", "", $_GET["order"] ) : "Title";
list( $NewsItems, $totalRows ) = NewsItem::getNewsItems( $start, PAGE_SIZE, $order );


?>

    <body>
        <!-- Preloader -->
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="#"><img src="img/sounduniverse_logo.svg" alt="Sound Universe" /></a>
                    </div>

                    <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Go back to the main page</a></li>

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
            </nav>
        </header>

        <!-- LANDING SECTION -->
        <section id="landing" style="height:40vh">
            <div class="container">
                <div class="row">
                    <h1>Read all the news</h1>
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <section class="row">
                <div class='col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12'>
                    <?php
                        $rowCount = 0;

                        foreach ( $NewsItems as $NewsItem ) {
                          $rowCount++;
                    ?>
                    <article id="newsitems_box" <?php if ( $rowCount % 2==0 ) echo ' class="alt"' ?>>
                        <div class="row">
                            <div id="news_date" class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                                <p>
                                    <?php echo $NewsItem->getValueEncoded( "uploadDate" ) ?>
                                </p>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
                                <h2><a href="view_newsitem.php?NewsItemId=<?php echo $NewsItem->getValueEncoded( "id" ) ?>&amp;start=<?php echo $start ?>&amp;order=<?php echo $order ?>"><?php echo $NewsItem->getValueEncoded( "title" ) ?></a></h2>
                                <p>
                                    <?php echo $NewsItem->getValueEncoded( "summary" ) ?>
                                </p>
                            </div>
                        </div>
                    </article>
                    <?php
                    }
                    ?>
                </div>
            </section>
        </div>

        <div style="width: 30em; margin-top: 20px; text-align: center;">
            <?php if ( $start > 0 ) { ?>
            <a href="view_newsitems.php?start=<?php echo max( $start - PAGE_SIZE, 0 ) ?>&amp;order=<?php echo $order ?>">Previous page</a>
            <?php } ?> &nbsp;
            <?php if ( $start + PAGE_SIZE < $totalRows ) { ?>
            <a href="view_newsitems.php?start=<?php echo min( $start + PAGE_SIZE, $totalRows ) ?>&amp;order=<?php echo $order ?>">Next page</a>
            <?php } ?>
        </div>

        <?php
            displayPageFooter();
        ?>

        <?php
            include ("footer.php");
        ?>
