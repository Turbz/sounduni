		<footer>
		    <div class="container">
			    <div class="row">
					<div class="footer_row col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<h4>Navigation</h4>
						<a class="footer_nav" href="#exhibition_text">Exhibition</a>
						<a class="footer_nav" href="#buy_tickets">Buy Ticket</a>
						<a class="footer_nav" href="#news_section">News</a>
						<a class="footer_nav" href="#aarhus2017_text">Aarhus 2017</a>
					</div>
					<div class="footer_row col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<h4>General</h4>
						<p>7600 Struer<br>
						Sondergade 23<br>
						<a href="mailto:mail@struermuseum.dk">mail@struermuseum.dk<br></a>
						<a href="tel:+4597851311"> +45 9785 1311</a>
						</p>
					</div>
					<div class="footer_row col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<h4>Opening hours</h4>
						<p>Tuesday to Friday<br>
						12:00 - 16:00<br>
						Saturday to Sunday<br>
						12:00 - 17:00
						</p>
					</div>

					<div class="footer_row col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<h4>Follow us on</h4>
						<a href="https://www.facebook.com/Struer-Museum-118625741934/?fref=ts" target="_blank"><img src="img/facebook_logo.png" alt="Facebook"></a>
						<a href="https://www.instagram.com/explore/tags/struermuseum/" target="_blank"><img src="img/instagram_logo.png" alt="Instagram"></a>
						<a href="https://www.tripadvisor.com/Attraction_Review-g659281-d3567382-Reviews-Struer_Museum-Struer_West_Jutland_Jutland.html" target="_blank"><img src="img/tripadvisor_logo.png" alt="Trip Advisor"></a>
					</div>
			    </div>
		    </div>
		</footer>
	<script type="text/javascript">
		window.sr = ScrollReveal();
		sr.reveal('.scroll_reveal');
	</script>
  </body>
</html>
