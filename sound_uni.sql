-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Vært: localhost
-- Genereringstid: 16. 12 2016 kl. 11:21:20
-- Serverversion: 5.6.26
-- PHP-version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sound_uni`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` smallint(5) unsigned NOT NULL,
  `uploadDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `content` mediumtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Data dump for tabellen `news`
--

INSERT INTO `news` (`id`, `uploadDate`, `title`, `summary`, `content`) VALUES
(1, '2016-11-30 11:56:43', 'my new title', 'I love dogs', 'I love dogs and cats'),
(2, '2016-12-14 08:10:44', 'Thisisverynais', 'nmiceieruwerewrfwerwern gfhjrtujedthre', 'dddfgwrefgergreze5ze5'),
(3, '2016-12-01 10:47:27', 'GodBlessYourSoulmate', '  i love everyone', 'is really bad noise'),
(4, '0000-00-00 00:00:00', 'xx', ' xx', 'xx'),
(8, '2016-11-30 18:19:53', 'hello ', 'its me ', 'karol'),
(9, '2016-11-30 19:26:43', 'test', '15641365456f41wre5f415er46 ', 're14g54v6etr41g54b1w4tsre5e,32gv1ws5r4et1bg54,estrz1532,bh1trswe,3'),
(10, '2016-11-30 19:42:25', 'rfwef', ' wefwefwe', 'fwefwefwe'),
(11, '2016-11-30 19:45:49', 'LL', 'efkjsklf ', 'lkfslkfs'),
(12, '2016-12-01 09:35:35', 'I love you bubbaaa', ' Why not can i not plz let me ', 'ferwergferfre  CUTE STICKERS'),
(13, '2016-12-13 16:38:14', 'Aarhus2056', 'My lovely little city Aarhus is gonna be the capital of culture in 2017. I don''t know what i really want to do on that day. ', 'My lovely little city Aarhus is gonna be the capital of culture in 2017. I don''t know what i really want to do on that day. '),
(14, '2016-12-08 20:46:19', 'Put Span put span Put Span put span ', ' Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span ', ' Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span '),
(15, '2016-12-08 20:49:03', 'AAAAAAAAAAAAAAAAAAAAAAAAAAA', '  Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span  Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span  Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span ', ' Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span  Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span  Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span Put Span put span ');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passwort` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `passwortcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwortcode_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data dump for tabellen `users`
--

INSERT INTO `users` (`id`, `email`, `passwort`, `vorname`, `nachname`, `created_at`, `updated_at`, `passwortcode`, `passwortcode_time`) VALUES
(1, 'struer@museum.dk', '$2y$10$kApKRqcb5wj.U6LBUE3m4O2ynb6rMj4tmE97RedVZwZMwo2hqcXrm', 'Thorben', 'Luepkes', '2016-11-30 18:01:49', NULL, 'fce1566ed93fc89caed2f21294d28760348fa746', '2016-12-01 08:53:42'),
(0, 'buba@bubu.com', '$2y$10$fQoSANTM71X9mnobfzp4eOxPFHkorkp/VHbLg1fQR2p3P49vMdGC6', 'Karol', 'Laski', '2016-12-08 18:20:18', NULL, NULL, NULL);

--
-- Begrænsninger for dumpede tabeller
--

--
-- Indeks for tabel `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Brug ikke AUTO_INCREMENT for slettede tabeller
--

--
-- Tilføj AUTO_INCREMENT i tabel `news`
--
ALTER TABLE `news`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
