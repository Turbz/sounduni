<?php

    // To handle POST form data
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $tickets    = $_POST['tickets'];
        $card       = $_POST['card'];
        $name       = $_POST['name'];
        $month      = $_POST['month'];
        $year       = $_POST['year'];
        $ccv        = $_POST['ccv'];

        // Check for empty fields
        // if any, then send http error response code
        if ( empty($card) OR empty($name) OR empty($month) OR empty($year) OR empty($ccv) ) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Woops! Some empty fieds are missing. Please fill out the remaining fields";
            exit;
        }

        // Receiver of the mail
        $to = "matthiashansen16@gmail.com";

        // Mail description field
        $subject = "Purchase of ticket from The Sound Universe";

        // mail content of variables
        $email_content = "You have received new purchasing information.\n\n"."Here are the details \n\n";
        $email_content .= "Tickets: $tickets\n\n";
        $email_content .= "Card Nr.: $card\n\n";
        $email_content .= "Name: $name\n\n";
        $email_content .= "Month: $month\n\n";
        $email_content .= "Year: $year\n\n";
        $email_content .= "CCV: $ccv\n\n";

        // Email header
        $email_headers = "From: sounduniverse@gmail.com";

        // if no errors, redirect to succes page
        if (mail($to, $subject, $email_content, $email_headers)) {
            header("Location: success.php");

        } else {
            // message not succeded
            http_response_code(500);
            echo "It seems that our server is not responding. Please try again";
        }

    } else {
        // if any inconvient errors should occur
        http_response_code(403);
        echo "Woops! It looks like we have some techinal difficulties";
    }

?>
