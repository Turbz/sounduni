<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <!-- TODO: make span lines same color as other thingy -->
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand hidden-xs" href="#video_section"><img src="img/sounduniverse_logo.png" alt="Sound Universe" /></a>
                    <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#landing"><img src="img/logo_mobile.png" alt="Sound Universe" /></a>
                </div>

                <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#exhibition_text">Exhibition</a></li>
                        <li><a href="#buy_tickets">Buy ticket</a></li>
                        <li><a href="#news_section">News</a></li>
                        <li><a href="#aarhus2017_text">Aarhus 2017</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
    </header>

    <main>
        <!-- LANDING SECTION DESKTOP -->
        <div id="video_section" class="hidden-xs hidden-sm">
            <video loop muted autoplay poster="img/videoframe.jpg" class="bg_video">
                <source src="img/bg_landing_video.mp4" type="video/mp4">
            </video>
            <article id="video_landing_text" class="container">
                <div class="row text-center col-md-12">
                    <h1>The Sound Universe</h1>
                    <p>
                        We do interactive sound experiences
                    </p>
                    <p>
                        using the newest sound tech on the market
                    </p>
                    <a href="#exhibition_text" class="btn btn-dark">Explore</a>
                </div>
            </article>
        </div>

        <!-- LANDING SECTION MOBILE/TABLET -->
        <section class="hidden-md hidden-lg" id="landing">
            <div class="container">
                <article class="row">
                    <h1> Sound Universe</h1>
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                        <p>
                            Struer Museum presents to you a whole new dimension of sound.
                        </p>
                    </div>
                    <a href="#exhibition_text" class="btn btn-dark">Explore</a>
                </article>
            </div>
        </section>

        <!-- MAIN CONTENT -->
        <div class="container main_container">
            <!-- SOUND EXHIBTION SECTION -->
            <section class="row scroll_reveal" id="purple_background">
                <article class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="exhibition_text">
                    <h2>Sound<br />Exhibition</h2>
                    <p class="sub_heading">The Sound Universe lets you explore various ways of interacting with sound. We offer new and exciting interactive sound experiences, which have never been seen in Denmark ever before</p>
                    <p class="sub_heading">You will experience a new perception of how both sound works but also how differently it can be interpretred</p>
                    <a href="#review_text" class="btn btn-light hidden-xs hidden-sm">See Reviews</a>
                    <p class="text-center hidden-md hidden-lg">
                        <a href="#review text" class="btn btn-light">See Reviews</a>
                    </p>
                </article>
                <figure class="col-lg-6 col-md-6 col-sm-6 hidden-xs" id="exhibition_picture">
                </figure>
            </section> <!-- Sound exhibition end -->

            <!-- REVIEWS SECTION -->
            <section class="row scroll_reveal" id="white_background">
                <h2>What people say</h2>

                <!-- MOBILE: REVIEW_SECTION -->
                <div id="review_carousel" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <article class="carousel-inner" role="listbox">
                        <!-- Slide 1 -->
                        <div class="item active">
                            <img src="img/review_picture_3.png" alt="Chania"> "Hairdresser's showpiece was the best experience. Can certainly recommend it!"
                        </div>
                        <!-- Slide 2 -->
                        <div class="item">
                            <img src="img/review_picture_1.png" alt="Chania"> "It is a very special museum, with many interesting objects to both see and try."
                        </div>
                        <!-- Slide 3 -->
                        <div class="item">
                            <img src="img/review_picture_2.png" alt="Flower"> ”It was well worth the trip to see and experience the many sound experiences.”
                        </div>
                    </article>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#review_carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#review_carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div> <!-- /#review_carousel -->

                <!-- DESKTOP: REVIEW_SECTION -->
                <div id="review_text" class="scroll_reveal">
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                        <p class="sub_heading">
                            See what our visitors had to say about the new Sound Universe museum.
                        </p>
                    </div>

                    <div class="review col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <article>
                            <img src="img/review_picture_3.png" alt="review person">
                            <p>
                                "Hairdresser's showpiece was the best experience. Can certainly recommend it!"
                            </p>
                        </article>
                    </div>

                    <div class="review col-lg-4 col-md-4 col-sm-6 hidden-xs">
                        <article>
                            <img src="img/review_picture_1.png" alt="review person">
                            <p>
                                "It is a very special museum, with many interesting objects to both see and try."
                            </p>
                        </article>
                    </div>

                    <div class="review col-lg-4 col-md-4 hidden-sm hidden-xs">
                        <article>
                            <img src="img/review_picture_2.png" alt="review person">
                            <p>
                                ”It was well worth the trip to see and experience the many sound experiences.”
                            </p>
                        </article>
                    </div>
                </div>
                <!-- /#review_text -->

                <a href="#buy_tickets" id="button" class="btn btn-dark text-left">Start now</a>
            </section> <!-- Reviews section end -->

            <!-- EXHIBITION NEWS SECTION -->
            <section class="row" id="exhibition_news">
                <div class="first_post col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <article class="article_post_one scroll_reveal">
                        <span class="article_head">
                            <h4><a data-popup-open="article_1" href="#">Get to know more about our new 3D sound</a></h4>
                            <p>Tune in and experience the 3D sound</p>
                        </span>
                    </article>

                    <div class="article_background"></div>

                    <div class="popup" data-popup="article_1">
                        <div class="popup_inner">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xd-12">
                                    <h2>Get to know more about our new 3D sound</h2>
                                    <p>
                                        Our most porpular exhibition item is currently the 3D Barber Shop sound experience.
                                    </p>
                                    <p>
                                        When attending the museum, you will be put into a original barber salon created by the Struer Museum Team. This staged invorenments especially helps to get in the right zone and setting the scene of the actual play.
                                    </p>
                                    <p>
                                        You can, at the moment, experience the same sound experience in our Pop up booth in Aarhus C, where you will be put in to the same scene as stated above. Come visit us and we will make sure you start to question, what is real and what is not.
                                    </p>
                                    <p>
                                        For the best experience of the provided audio, please plug in your headset, preferably the best set you have, to create the best scene possible. When plugged in and ready to go, push the play button and close your eyes as the narrator will instructor you on further. We hope you have fun.
                                    </p>
                                    <p>
                                        // The Sound Universe Team
                                    </p>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-hidden">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/K2CQFbTpQo0"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="popup_close" data-popup-close="article_1" href="#">x</a>
                    </div>
                </div>

                <div class="second_post col-lg-6 col-md-6 col-sm-6 hidden-xs">
                    <article class="article_post_two scroll_reveal">
                        <span class="article_head">
                            <h4>
                                <a data-popup-open="article_2" href="#">
                                How is sound really made?
                            <br />
                                See how here.
                                </a>
                            </h4>
                            <p>This is a short describtion about the news article</p>
                        </span>
                    </article>
                    <div class="article_background"></div>

                    <div class="popup" data-popup="article_2">
                        <div class="popup_inner">
                            <article class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xd-12">
                                    <h2>Donec egestas vel mauris id mattis.</h2>
                                    <p>
                                        Vestibulum eros nibh, viverra ac dolor eget, dapibus consectetur quam. Ut suscipit ex eget convallis consequat. Integer sit amet varius justo. Donec id libero ornare, eleifend magna vitae, gravida lacus. Cras vitae magna eu lectus pretium mollis molestie
                                        non nunc. Praesent egestas consequat lorem eu volutpat. Mauris et urna tincidunt, viverra turpis quis, suscipit lacus. Mauris in ultricies ante. Quisque metus ante, maximus sit amet lacinia eget, venenatis ut mauris.
                                        Duis rhoncus non metus sed varius. Fusce id felis rutrum, suscipit nisi ut, ornare turpis. In varius bibendum fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas
                                        eu quam ipsum. Nullam imperdiet vel mi ut venenatis.
                                    </p>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-hidden">
                                    <img class="img-responsive center-block" src="img/gallery_1.jpg" alt="Flower">
                                </div>
                            </article>
                        </div>
                        <a class="popup_close" data-popup-close="article_2" href="#">x</a>
                    </div>
                </div>
            </section> <!-- Exhibtion news end -->

            <!-- IMAGE GALLERY -->
            <section class="row scroll_reveal">
                <div id="Gallery" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#Gallery" data-slide-to="0" class="active"></li>
                        <li data-target="#Gallery" data-slide-to="1"></li>
                        <li data-target="#Gallery" data-slide-to="2"></li>
                        <li data-target="#Gallery" data-slide-to="3"></li>
                        <li data-target="#Gallery" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img class="img-responsive center-block" src="img/gallery_1.jpg" alt="Girl listening to sound">
                        </div>
                        <div class="item">
                            <img class="img-responsive center-block" src="img/gallery_2.jpg" alt="Ear sculpture">
                        </div>
                        <div class="item">
                            <img class="img-responsive center-block" src="img/gallery_3.jpg" alt="Barber Shop 3D sound">
                        </div>
                        <div class="item">
                            <img class="img-responsive center-block" src="img/gallery_4.jpg" alt="Radio Wall">
                        </div>
                        <div class="item">
                            <img class="img-responsive center-block" src="img/gallery_5.jpg" alt="B&O exhibition">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#Gallery" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#Gallery" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </section> <!-- Image gallery end -->

            <!-- BUY TICKET SECTION -->
            <section class="row scroll_reveal buy_tickets" id="purple_background">
                <h2 class="text-center" id="buy_tickets">Buy Ticket</h2>
                <p class="sub_heading text-center">
                    Our ticket is a combination of both transport, museum ticket and food.<br/> We want you to spend minimum time on searching and more on exploring
                </p>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <article class="ticket_box">
                        <img src="img/train.svg" alt="Chania">
                        <h4>Transport</h4>
                        <p>
                            You will receive a transportation ticket by mail on how to get from Aarhus to Struer by ARRIVA Trains
                        </p>
                    </article>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <article class="ticket_box">
                        <img src="img/ticket.svg" alt="Chania">
                        <h4>Ticket</h4>
                        <p>
                            A ticket for The Sound Universe will be provided, which gives you access to all exhibition items
                        </p>
                    </article>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <article class="ticket_box">
                        <img src="img/fish.svg" alt="Chania">
                        <h4>Food</h4>
                        <p>
                            Food at the museum will be provided in the price in order to make sure you stay fully energized
                        </p>
                    </article>
                </div>
                <a href="ticket.php" class="btn btn-light text-center">Buy now</a>
            </section> <!-- Buy ticket end -->

            <!-- NEWS SECTION -->
            <section class="row scroll_reveal" id="white_background">
                <?php
            		require_once( "admin/common.inc.php" );
            		require_once( "admin/config.php" );
            		require_once( "admin/NewsItem.class.php" );

            		$start = isset( $_GET["start"] ) ? (int)$_GET["start"] : 0;
            		$order = isset( $_GET["order"] ) ? preg_replace( "/[^ a-zA-Z]/", "", $_GET["order"] ) : "uploadDate";
            		list( $NewsItems, $totalRows ) = NewsItem::getNewsItems( $start, PAGE_SIZE, $order  );
    			?>

                    <h2 id="news_section">News</h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                        <p class="sub_heading">
                            Follow our latest news about The Sound Universe Exhibition.
                            <br /> We will keep you update on upcomming events and news from the museum.
                        </p>
                    </div>
                    <?php
        			$rowCount = 0;

        			foreach ( $NewsItems as $NewsItem ) {
        			  $rowCount++;
    			?>
                        <tr<?php if ( $rowCount % 2==0 ) echo ' class="alt"' ?>></tr>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <article class="news_post">
                                    <div class="news_head">
                                        <h4><a href="view_newsitem.php?NewsItemId=<?php echo $NewsItem->getValueEncoded( "id" ) ?>&amp;start=<?php echo $start ?>&amp;order=<?php echo $order ?>"><?php echo $NewsItem->getValueEncoded( "title" ) ?></a></h4>
                                        <p class="small">
                                            <?php echo $NewsItem->getValueEncoded( "uploadDate" ) ?>
                                        </p>
                                        <p>
                                            <?php echo $NewsItem->getValueEncoded( "summary" ) ?>
                                        </p>

                                    </div>
                                </article>
                            </div>
                            <?php
				    }
				?>
                                <a href="view_newsitems.php" id="button" class="btn btn-dark">Read more news</a>
            </section> <!-- News end -->

            <!-- INSTAGRAM SECTION -->
            <section class="row scroll_reveal" id="white_background">
                <h2>Instagram</h2>
                <p class="sub_heading">
                    Join us at our instagram and see some of our best images from The Sound Exhibition.
                    <br /> Share your images with us by using the hashtag #MySoundUniverse.
                </p>
                <div class="grid">
                    <div class="item hidden-xs">
                        <a href="https://www.instagram.com/p/BN_-L0ggz5Y/?taken-by=_sounduniverse" target="_blank"><img src="img/instagram_6.jpg" alt="Instagram 6"></a>
                    </div>
                    <div class="item hidden-xs">
                        <a href="https://www.instagram.com/p/BN_8jcQAcIg/?taken-by=_sounduniverse" target="_blank"><img src="img/instagram_8.jpg" alt="Instagram 8"></a>
                    </div>
                    <div class="item">
                        <a href="https://www.instagram.com/p/BN_73WPgWWg/?taken-by=_sounduniverse" target="_blank"><img src="img/instagram_1.jpg" alt="Instagram 1"></a>
                    </div>
                    <div class="item hidden-xs">
                        <a href="https://www.instagram.com/p/BN_8bopgBPG/?taken-by=_sounduniverse" target="_blank"><img src="img/instagram_2.jpg" alt="Instagram 2"></a>
                    </div>
                </div>
                <a href="https://www.instagram.com/_sounduniverse/" class="btn btn-dark" target="_blank">See more images</a>
            </section> <!-- Instagram section end -->

            <!-- AARHUS 2017 SECTION -->
            <section class="row scroll_reveal" id="purple_background">
                <article class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="aarhus2017_text">
                    <h2>Aarhus 2017<br />Project</h2>
                    <p class="sub_heading">
                        Throughout 2017, Aarhus will be hosts of this year’s European Capital of Culture. Various events will take place to celebrate culture in our society.
                    </p>
                    <p class="sub_heading">Join us for this year’s celebration.</p>
                    <a href="http://www.aarhus2017.dk/en/" class="btn btn-light" target="_blank">Learn more</a>
                </article>

                <figure class="col-lg-6 col-md-6 col-sm-6 hidden-xs scroll_reveal" id="aarhus2017_picture">
                </figure>
            </section> <!-- Aarhus 2017 end -->

            <!-- Google Maps Section -->
            <section class="row scroll_reveal hidden-xs">
                <div id="map"></div>
            </section>

            <!-- Google Maps Api -->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuWaYu9cE0lau4-J3ih6ZbduPNqs0nOgw&callback=initMap" async defer>
            </script>
        </div>
    </main>
