<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Purchase Ticket – Struer Museum</title>
    <meta name="description" content="Buy a museum ticket for the Sound Universe in Struer and experience interactive exhibition items.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
      <div id="status">&nbsp;</div>
    </div>
    <main id="purchase_ticket" class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h1 class="text-center">Purchase Ticket</h1>
                <p class="text-center">
                    In order to fully purchase the museum ticket, you are to fill in your credentials to proceed with the order.
                </p>

                <!-- Ticket form  -->
                <form id="contact_form" method="post" action="buy_ticket.php" role="form">
                    <div class="messages"></div>

                    <div class="controls">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleSelect1">Number of tickets</label>
                                    <p class="small_label">
                                        18 and below gets free entrance and transport. Buy ticket at museum
                                    </p>
                                    <select class="form-control" name="tickets">
                                        <option value="199">1 ticket</option>
                                        <option value="398">2 tickets</option>
                                        <option value="570">3 tickets – 5% discount</option>
                                        <option value="690">4 tickets – 10% discount</option>
                                        <option value="845">5 tickets – 15% discount</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- Card number input -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_number">Card number</label>
                                    <input id="form_number" type="text" name="card" class="form-control number_only" maxlength="16" placeholder="Enter your card number" required="required" data-error="Credit card credentials is required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Month input -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="small_label" for="form_month">Expiry Month</label>
                                    <input id="form_month" type="text" name="month" class="form-control number_only" placeholder="MM" required="required" maxlength="2" data-error="Valid data is required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <!-- Year input -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="small_label number_only" for="form_year">Expiry Year</label>
                                    <input id="form_year" type="text" name="year" class="form-control" placeholder="YY" required="required" maxlength="2" data-error="Valid data is required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <!-- CCV input -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="small_label number_only" for="form_cvc">CCV</label>
                                    <input id="form_ccv" type="text" name="ccv" class="form-control" placeholder="CCV" required="required" maxlength="3" data-error="Enter 3 digit CCV">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <!-- Credit Card icon -->
                            <div class="col-md-2 col-mf-offset-1 hidden-xs hidden-sm">
                                <img class="credit_card_icon" src="img/creditcard-outline-filled.png" alt="">
                            </div>
                        </div>
                        <div class="row">
                            <!-- Name input -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_name">Card Name</label>
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Enter card holder name" required="required" data-error="Full name is required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <!-- Total price account -->
                        <div class="row price_column">
                            <div class="col-md-5">
                                <p>Total Price: </p>
                            </div>
                            <div class="col-md-6">
                                <p><span id='total_sum'>199</span> kr</p>
                            </div>
                        </div>
                        <div class="row button_mobile">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-dark">Purchase</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Go back link -->
        <div id="goback_link" class="row text-center">
            <a href="index.php">Go back to front page</a>
        </div>
    </main>

    <script type="text/javascript">
        // Calculate the total sum of selected tickets
        jQuery(document).ready(function() {
            $('select').change(function(){
                var sum = 0;
                $('select :selected').each(function() {
                    sum += Number($(this).val());
                });
                 $("#total_sum").html(sum);
            });
        });
    </script>
  </body>
</html>
