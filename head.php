<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sound Universe - Struer Museum</title>
    <meta name="keywords" content="Sound Universe, Struer Museum, Aarhus 2017, interactive experience, interactive exhibition, sound experience, bang & olufsen, B&O, Bang and Olufsen">
    <meta name="description" content="The Sound Universe offers a unique sound universe experience.">
    <meta name="author" content="Struer Museum">
    <meta name="dcterms.rightsHolder" content="Struer Museum">
    <meta property="og:title" content="The Sound Exhibition - Struer Museum">
    <meta property="og:description" content="The Sound Universe offers a unique sound universe experience.">
    <meta property="og:image" content="http://134.255.234.196/school/sound_uni/img/sound_universe_thumbnail.png">
    <meta property="og:url" content="http://134.255.234.196/school/sound_uni">
    <meta property="og:type" content="website">    
    <meta property="og:image" content="#">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<!-- <script>
    (function(b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function() {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X', 'auto');
    ga('send', 'pageview');
</script> -->
