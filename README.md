# Project 3.2

This projected was developed for Struer Museum's newest exhibition, *The Sound Universe*, which was made for the Aarhus 2017 Project. You can visit the site at the following link: [The Sound Universe Website](http://134.255.234.196/sound_uni/)


## The final project folder

Through gulp, is the final destination folder called `dist` created with all the compressed images, concatenated JS and CSS files. This final build folder is fully compressed and should be the one deployed for online servers.

When working on the code, never work in the `dist` folder. Only work in the root folder of the cloned project and change the desired files.



## Bror's access to admin panel

1. Go to http://134.255.234.196/sound_uni/admin/login.php

2. Username: struer@museum.dk

3. Password: akaishu2


## Features included

- Gulp
    - Preprocessor (SASS)
    - compress and minify CSS
    - Minify images to reduce file size
    - browserSync to refresh browser on save in editor
    - Concatenating CSS and JS files into one for faster load time
- Bootstrap (SASS version)
- SASS


## Get started

### Installing

1. Download or clone this repo

You'll need to have Gulp installed. If you already have Gulp installed, you can skip the following line. To install gulp simply run the following line in your terminal:

2. `npm install -g gulp`

3. `npm install`


### Run the server

SASS is being compiled by ruby, which is not installed on Windows PCs by default. You will need this to be installed to run the gulp commands properly.

1. `gulp` - to minify and compress all files

2. `gulp watch` - to both compress and create localhost working environment for development


### Kristina's easy piecy guide

1. Run MAMPP and start servers

2. open terminal

3. `cd desktop/sounduni`

4. Run `gulp` and `gulp watch`

5. Change your localhost port to :8888


## Tasks done for the project report

### Communication

- [x] A campaign plan that is based on the purpose and goals of your case. You must base your campaign plan on the knowlege of the pull society
- [x] Create core content for the case you have chosen.
- [x] Create 3 personas based on research about your target audience. Use the personas to create relevant content and relate the personas to the event/ experience or other activity you are going to create.
- [x] Staging of a specific event, experience or other activities designed to involve and engage the target audience in a memorable way. You must clearly explain how these activities are consistent with and support the activities described in campaign plan.

### Design

- [x] A convincing campaign website with its own universe
- [x] Manage and create the correlation between message and design
- [x] Apply methods for documentation of interactive multimedia productions
- [x] Manage design and development of interactive user interfaces
- [x] Apply methods and tools to model, structure, and implement functionality.  
- [x] Apply production and post production techniques to video/animation
- [x] Design manual

### Interaction

The solution must demonstrate that the group is able to develop a solution that makes use of object-orientated php and a well-structured database.
- [x] The database must contain several entities and several different types of relations
- [x] The solution must demonstrate basic as well as complex use of SQL
- [x] The code must be well-structured and indented hierarchically
- [x] The code must contain comments and be legible
- [x] The HTML code must be w3c validated
- [x] Use variable names that reappear in HTML formulas, in the php code and in the column names of the tables in the database.

The solution must be thoroughly documented through:
- [x] An E/R diagram that describes the complex solution.
- [x] An account of how the E/R design is converted into tables.
- [x] A structured description of the entire application, database structure, including a description of the interrelationships of primary keys and foreign keys and an explanation of the SQL applied.
- [x] An account of security considerations and measures.
- [x] An account of the application of all other components, techniques and code libraries.


## The Dream Team

* **Thorben Lüpkes**
* **Kristina Jakaviciute**
* **Karolina Łaski**
* **Matthias Hansen**
