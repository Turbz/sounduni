<?php 
session_start();
session_destroy();
unset($_SESSION['userid']);

//Remove Cookies
setcookie("identifier","",time()-(3600*24*365)); 
setcookie("securitytoken","",time()-(3600*24*365)); 
//Include necessary scripts
require_once("config.inc.php");
require_once("functions.inc.php");

?>

<div class="container main-container">
Logout successful! <a href="login.php">Back to Login</a>.
</div>
