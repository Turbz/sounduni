<?php
session_start();
require_once("config.inc.php");
require_once("functions.inc.php");
include ("admincp_head.php");
//Check if user is Logged In
//Put this on every internal site!
$user = check_user();

?>

<header>
    <div class="container">
        <nav class="navbar navbar-default navbar-fixed-top">
	        <div class="container">
	            <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="logout.php">Logout</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
            </div>
	    </nav>
    </div>
</header>

<main>
    <section id="admin_internal">
        <div class="row text-center">
		    <h1>Welcome <?php echo htmlentities($user['vorname']); ?>, you are now signed in</h1>
	    </div>
    </section>

    <div class="container">
      <section>
            <div id="admin_controls" class="row">
                <div id="admin_gallery_upload" class="text-center col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h2>Upload images</h2>
        			<p>
                        To apply and use images on the website, choose a image you wish to upload from your computer and afterwards click on the 'Upload' button listed below.
                    </p>
                    <p>
                        To use images throughout the site you can reference to them in the code from the 'img' folder. An example of a complete path would be 'img/sounduniverse.png'.
                    </p>
        			<form action="upload.php" method="post" enctype="multipart/form-data">
                		<input class="btn btn-light" type="file" name="datei">
                		<input class="btn btn-light" type="submit" value="Upload Image">
        	 		</form>
                </div>
                <div id="admin_news_upload" class="text-center col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h2>Upload news</h2>
        			<p>
                        For uploading a new article to The Sound Universe Website, please click on the 'Add News' button listed below.
                    </p>
                    <p>
                        You can edit your already uploaded news in the below table by clicking on the news article title, which redirects you to the 'Edit News' page. Save changes and it will change instantly on the website.
                    </p>
        			<a href="addnews.php" class="btn btn-light">Add News</a>
                </div>
    		</div>


    	    <div id="news_table" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    			<h2 class="text-center">List of all published news</h2>

    			<!-- Display the latest news -->
    			<?php
                //Include necessary scripts
    				require_once( "common.inc.php" );
    				require_once( "config.php" );
    				require_once( "NewsItem.class.php" );
    				$start = isset( $_GET["start"] ) ? (int)$_GET["start"] : 0;
    				$order = isset( $_GET["order"] ) ? preg_replace( "/[^ a-zA-Z]/", "", $_GET["order"] ) : "uploadDate";
    				list( $NewsItems, $totalRows ) = NewsItem::getNewsItems( $start, 4, $order );
    			?>

    			<!-- Put everything in one table  -->
    		    <table id='admin_news' class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        	        <tr>
        		        <th class="col-lg-4 col-md-4 col-sm-6 col-xs-12">Title</th>
        		        <th class="col-lg-4 col-md-4 hidden-sm hidden-xs">Summary</th>
        		        <th class="col-lg-4 col-md-4 col-sm-6 hidden-xs">Upload Date</th>
        	        </tr>

                    <?php
                		$rowCount = 0;
                		foreach ( $NewsItems as $NewsItem ) {
                		$rowCount++;
        			?>
        	        <tr<?php if ( $rowCount % 1 == 0 ) echo ' class="alt"' ?>>
            	        <td class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><a href="edit_newsitem.php?NewsItemId=<?php echo $NewsItem->getValueEncoded( "id" ) ?>&amp;start=<?php echo $start ?>&amp;order=<?php echo $order ?>"><?php echo $NewsItem->getValueEncoded( "title" ) ?></a></td>
            	        <td class="col-lg-4 col-md-4 hidden-sm hidden-xs"><?php echo $NewsItem->getValueEncoded( "summary" ) ?></td>
            	        <td class="col-lg-4 col-md-4 col-sm-6 hidden-xs"><?php echo $NewsItem->getValueEncoded( "uploadDate" ) ?></td>
        	        </tr>
        			<?php
        			}
        			?>
    			</table>
                <p class="text-center">
                    <a class="btn btn-dark text-center" href="edit_newsitems.php">See all news</a>
                </p>
    		</div>
        </div>
    </section>
</main>
