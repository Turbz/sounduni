<?php
//Include necessary scripts
session_start();
require_once("config.inc.php");
require_once("functions.inc.php");
include ("admincp_head.php");

$error_msg = "";
if(isset($_POST['email']) && isset($_POST['passwort'])) {
	$email = $_POST['email'];
	$passwort = $_POST['passwort'];
	//Bind and prepare
	$statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
	$result = $statement->execute(array('email' => $email));
	$user = $statement->fetch();

	//Check passwords
	if ($user !== false && password_verify($passwort, $user['passwort'])) {
		$_SESSION['userid'] = $user['id'];

		//Stay logged in
		if(isset($_POST['Keep me logged in!'])) {
			$identifier = random_string();
			$securitytoken = random_string();

			$insert = $pdo->prepare("INSERT INTO securitytokens (user_id, identifier, securitytoken) VALUES (:user_id, :identifier, :securitytoken)");
			$insert->execute(array('user_id' => $user['id'], 'identifier' => $identifier, 'securitytoken' => sha1($securitytoken)));
			setcookie("identifier",$identifier,time()+(3600*24*365)); //Valid for 1 year
			setcookie("securitytoken",$securitytoken,time()+(3600*24*365)); //Valid for 1 year
		}

		header("location: internal.php");
		exit;
	} else {
		$error_msg =  "E-Mail oder Passwort war ungültig<br><br>";
	}

}

$email_value = "";
if(isset($_POST['email']))
	$email_value = htmlentities($_POST['email']);


?>

<div class="container" id="admin_cp">
	<section class="row" id="login_page">
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="login_text">
	       <h2>Struer Museum Admin Panel</h2>
	         <form action="login.php" method="post">




<?php
if(isset($error_msg) && !empty($error_msg)) {
	echo $error_msg;
}
?>

	<label for="inputEmail" class="sr-only">E-Mail</label>
	<input type="email" name="email" id="inputEmail" class="form-control" placeholder="E-Mail" value="<?php echo $email_value; ?>" required autofocus>
	<label for="inputPassword" class="sr-only">Password</label>
	<input type="password" name="passwort" id="inputPassword" class="form-control" placeholder="Password" required>
	<div class="checkbox">
	  <label>
		<input type="checkbox" value="remember-me" name="angemeldet_bleiben" value="1" checked> Keep me logged in!
	  </label>
	</div>
	<button class="btn btn-dark" type="submit">Login</button>
	<br>
	<a href="passwortvergessen.php">Forgot password?</a>
  </form>
  </div>
</section>
</div> <!-- /container -->
