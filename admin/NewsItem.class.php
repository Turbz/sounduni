<?php

require_once "DataObject.class.php";

class NewsItem extends DataObject {

  protected $data = array(
    "id" => "",
    "uploadDate" => "",
    "title" => "",
    "summary" => "",
    "content" => ""

  );



  public static function getNewsItems( $startRow, $numRows, $order ) {
    // Get connection to database
    $conn = parent::connect();
    // extracts all data from TBL_NEWS
    $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " . TBL_NEWS . " ORDER BY $order DESC LIMIT :startRow, :numRows";

    // SQL statements are useful against SQL injections, because parameter values, which are transmitted later using a different protocol need not to
    // be correctly escaped
    try {
      // Prepare: SQL statement template is created and sent to the database
      // Databases parses, compiles and performs query optimization on the SQL statement template -> stores result without executing
      $st = $conn->prepare( $sql );

      // bindValue: binds the values to parameters, and are ready to be executed
      $st->bindValue( ":startRow", $startRow, PDO::PARAM_INT );
      $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );

      // Execute: execute the statement which was created and binded to parameters
      $st->execute();
      $NewsItems = array();
      foreach ( $st->fetchAll() as $row ) {
        $NewsItems[] = new NewsItem( $row );
      }
      // totalRows variable displayed in view_newsitems.php
      $st = $conn->query( "SELECT found_rows() as totalRows" );
      $row = $st->fetch();
      parent::disconnect( $conn );
      return array( $NewsItems, $row["totalRows"] );
    }
      // Closing the database connection when data is retrieved
      catch ( PDOException $e ) {
      parent::disconnect( $conn );
      die( "Query failed: " . $e->getMessage() );
    }
  }

  public static function getNewsItem( $id ) {
    $conn = parent::connect();
    $sql = "SELECT * FROM " . TBL_NEWS . " WHERE id = :id";

    try {
      $st = $conn->prepare( $sql );
      $st->bindValue( ":id", $id, PDO::PARAM_INT );
      $st->execute();
      $row = $st->fetch();
      parent::disconnect( $conn );
      if ( $row ) return new NewsItem( $row );
    } catch ( PDOException $e ) {
      parent::disconnect( $conn );
      die( "Query failed: " . $e->getMessage() );
    }
  }

  public function insert() {
    $conn = parent::connect();
    $sql = "INSERT INTO " . TBL_NEWS . " (
              title,
              summary,
              content

            ) VALUES (
              :title,
              :summary,
              :content
            )";

    try {
      $st = $conn->prepare( $sql );

      $st->bindValue( ":title", $this->data["title"], PDO::PARAM_STR );
      $st->bindValue( ":summary", $this->data["summary"], PDO::PARAM_STR );
      $st->bindValue( ":content", $this->data["content"], PDO::PARAM_STR );
      $st->execute();
      parent::disconnect( $conn );
    } catch ( PDOException $e ) {
      parent::disconnect( $conn );
      die( "Query failed: " . $e->getMessage() );
    }
  }

  public function update() {
    $conn = parent::connect();
    $sql = "UPDATE " . TBL_NEWS . " SET
              title = :title,
              summary = :summary,
              content = :content
              WHERE id = :id";

    try {
      $st = $conn->prepare( $sql );
      $st->bindValue( ":id", $this->data["id"], PDO::PARAM_INT );
      $st->bindValue( ":title", $this->data["title"], PDO::PARAM_STR );
      $st->bindValue( ":summary", $this->data["summary"], PDO::PARAM_STR );
      $st->bindValue( ":content", $this->data["content"], PDO::PARAM_STR );
      $st->execute();
      parent::disconnect( $conn );
    } catch ( PDOException $e ) {
      parent::disconnect( $conn );
      die( "Query failed: " . $e->getMessage() );
    }
  }


  // Deleting rows of data
  public function delete() {
    $conn = parent::connect();
    $sql = "DELETE FROM " . TBL_NEWS . " WHERE id = :id";

    try {
      $st = $conn->prepare( $sql );
      $st->bindValue( ":id", $this->data["id"], PDO::PARAM_INT );
      $st->execute();
      parent::disconnect( $conn );
    } catch ( PDOException $e ) {
      parent::disconnect( $conn );
      die( "Query failed: " . $e->getMessage() );
    }
  }



}

?>
