<!-- Include the styles for the admin-CP -->
<!doctype html>
<html >
<head>
	<!-- Charset and meta, same as main-header, just different stylesheet routing -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Struer Museum Admin-CP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Folder rerouting for admin panel -->
    <link rel="stylesheet" href="../css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>

</head>
<body>
