<?php
session_start();
//Include necessary scripts
require_once("config.inc.php");
require_once("functions.inc.php");
require_once("common.inc.php");
include ("admincp_head.php");

//Check, if user is logged in
$user = check_user();

if ( isset( $_POST["action"] ) and $_POST["action"] == "register" ) {
  processForm();
} else {
  displayForm( array(), array(), new NewsItem( array() ) );
}

function displayForm( $errorMessages, $missingFields, $NewsItem ) {


  if ( $errorMessages ) {
    foreach ( $errorMessages as $errorMessage ) {
      echo $errorMessage;
    }
  }
else {
?>

<?php } ?>
<header>
    <div class="container">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="internal.php">Main</a></li>
                        <li><a href="edit_NewsItems.php">Edit all</a></li>
                        <li><a href="logout.php">Logout</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
         </nav>
    </div>
</header>

<main>
    <section id="admin_internal">
        <div class="row text-center">
            <h1>Add news</h1>
        </div>
    </section>
        <!-- Container -->
        <div class="container">
        <!-- Boostrap row -->
            <section class="row">
        <!-- Bootstrap columns -->
                <div id="edit_form" class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3'>
                    <div class="controls">
              <!-- Heading -->
                        <h1>Add news</h1>
                        <form action="addnews.php" method="post" style="margin-bottom: 50px;">
                            <input type="hidden" name="action" value="register" />
                            <!-- Form -->
                            <div class="form-group">
                                <label for="title"<?php validateField( "title", $missingFields ) ?>>Title</label>
                                <input class="form-control" type="text" name="title" id="title" value="<?php echo $NewsItem->getValueEncoded( "title" ) ?>" />
                            </div>
                            <div class="form-group">
                                <label for="summary">Summary</label>
                                <textarea class="form-control" name="summary" id="summary" rows="6" cols="50"> <?php echo $NewsItem->getValueEncoded( "summary" ) ?></textarea>
                            </div>
                            <div class="form-group">
                                 <label for="content">Content</label>
                                <textarea class="form-control" name="content" id="content" rows="4" cols="50"><?php echo $NewsItem->getValueEncoded( "content" ) ?></textarea>
                            </div>
                            <div style="clear: both;">
                              <input type="submit" name="submitButton" id="submitButton" value="Send Details" />
                              <input type="reset" name="resetButton" id="resetButton" value="Reset Form" style="margin-right: 20px;" />
                               <ul class="nav navbar-nav navbar-right">
                                   <!-- Lougout -->
                                   <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>

<?php
  displayPageFooter();
}

function processForm() {
  $requiredFields = array( "title" );
  $missingFields = array();
  $errorMessages = array();

  $NewsItem = new NewsItem( array(
    "title" => isset( $_POST["title"] ) ? preg_replace( "/[^ \-\_a-zA-Z0-9]/", "", $_POST["title"] ) : "",
    "summary" => isset( $_POST["summary"] ) ? preg_replace( "/[^ \'\,\.\-a-zA-Z0-9]/", "", $_POST["summary"] ) : "",
    "content" => isset( $_POST["content"] ) ? preg_replace( "/[^ \'\,\.\-a-zA-Z0-9]/", "", $_POST["content"] ) : ""

    ));

  foreach ( $requiredFields as $requiredField ) {
    if ( !$NewsItem->getValue( $requiredField ) ) {
      $missingFields[] = $requiredField;
    }
  }

  if ( $missingFields ) {
    $errorMessages[] = '<p class="error">There were some missing fields in the form you submitted. Please complete the fields highlighted below and click Send Details to resend the form.</p>';
  }

  if ( $errorMessages ) {
    displayForm( $errorMessages, $missingFields, $NewsItem );
  } else {
    $NewsItem->insert();
    displayThanks();
  }
}

function displayThanks() {
  displayPageHeader( "News submitted!" );
?>
    <a href="addnews.php">Go back</a>
<?php
  displayPageFooter();
}
?>
