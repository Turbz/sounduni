<?php
//Include necessary scripts to make editing news work
require_once("functions.inc.php");
require_once( "common.inc.php" );
require_once( "config.php" );
require_once( "NewsItem.class.php" );
include ("admincp_head.php");

$NewsItemId = isset( $_REQUEST["NewsItemId"] ) ? (int)$_REQUEST["NewsItemId"] : 0;

if ( !$NewsItem = NewsItem::getNewsItem( $NewsItemId ) ) {
  displayPageHeader( "Error" );
  echo "<div>NewsItem not found.</div>";
  displayPageFooter();
  exit;
}

if ( isset( $_POST["action"] ) and $_POST["action"] == "Save Changes" ) {
  saveNewsItem();
} elseif ( isset( $_POST["action"] ) and $_POST["action"] == "Delete NewsItem" ) {
  deleteNewsItem();
} else {
  displayForm( array(), array(), $NewsItem );
}

function displayForm( $errorMessages, $missingFields, $NewsItem ) {


  if ( $errorMessages ) {
    foreach ( $errorMessages as $errorMessage ) {
      echo $errorMessage;
    }
  }

  $start = isset( $_REQUEST["start"] ) ? (int)$_REQUEST["start"] : 0;
  $order = isset( $_REQUEST["order"] ) ? preg_replace( "/[^ a-zA-Z]/", "", $_REQUEST["order"] ) : "title";
?>
<header>
    <div class="container">
      <!-- Bootstrap navigation -->
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
              <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="internal.php">Main</a></li>
                    <li><a href="edit_NewsItems.php">Edit all</a></li>
                    <li><a href="logout.php">Logout</a></li>
                  </ul>
                </div><!-- /.navbar-collapse Bootstrap -->
            </div>
      </nav>
    </div>
</header>
<main>
<!-- Start admin internal section -->
      <section id="admin_internal">
          <div class="row text-center">
             <h1><?php displayPageHeader( "edit News: " . $NewsItem->getValueEncoded( "title" ) ); ?></h1>
          </div>
      </section>
<!-- End admin internal section -->
        <div class="container">
        <!-- Bootstrap row -->
          <section class="row">
        <!-- Bootstrap columns -->
          <div id="edit_form" class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3'>
              <div class="controls">
              <h2> <?php displayPageHeader( "Edit news: " . $NewsItem->getValueEncoded( "title" ) ); ?></h2>
                <form action="edit_NewsItem.php" method="post">

                    <input type="hidden" name="NewsItemId" id="NewsItemId" value="<?php echo $NewsItem->getValueEncoded( "id" ) ?>" />
                    
                    <div class="form-group">
                      <label for="title"<?php validateField( "title", $missingFields ) ?>>Title </label>
                      <input class="form-control" type="text" name="title" id="title" value="<?php echo $NewsItem->getValueEncoded( "title" ) ?>" />
                    </div>
                    <div class="form-group">
                      <label for="summary"<?php validateField( "summary", $missingFields ) ?>>Summary</label>
                      <textarea rows="4" class="form-control" name="summary" id="summary"> <?php echo $NewsItem->getValueEncoded( "summary" ) ?></textarea>
                    </div>
                    <div class="form-group">
                      <label for="content"<?php validateField( "content", $missingFields ) ?>>Content</label>
                      <textarea rows="10" class="form-control" name="content" id="content"><?php echo $NewsItem->getValueEncoded( "content" ) ?></textarea>
                    </div>
                    <div style="clear: both;">
                      <input type="submit" name="action" id="saveButton" value="Save Changes" />
                      <input type="submit" name="action" id="deleteButton" value="Delete NewsItem" style="margin-right: 20px;" />
                    </div>
                </form>
              </div>
          </div>
          
        </section>
      </div>

<?php
  displayPageFooter();
}
//Save the News
function saveNewsItem() {
  $requiredFields = array( "title", "summary", "content" );
  $missingFields = array();
  $errorMessages = array();

  $NewsItem = new NewsItem( array(
    "id" => isset( $_POST["NewsItemId"] ) ? (int) $_POST["NewsItemId"] : "",
    "title" => isset( $_POST["title"] ) ? preg_replace( "/[^a-zA-Z0-9]/", "", $_POST["title"] ) : "",
    "summary" => isset( $_POST["summary"] ) ? preg_replace( "/[^ \'\,\.\-a-zA-Z0-9]/", "", $_POST["summary"] ) : "",
    "content" => isset( $_POST["content"] ) ? preg_replace( "/[^ \'\,\.\-a-zA-Z0-9]/", "", $_POST["content"] ) : ""
  ) );

  foreach ( $requiredFields as $requiredField ) {
    if ( !$NewsItem->getValue( $requiredField ) ) {
      $missingFields[] = $requiredField;
    }
  }

  if ( $missingFields ) {
    $errorMessages[] = '<p class="error">There were some missing fields in the form you submitted. Please complete the fields highlighted below and click Send Details to resend the form.</p>';
  }

  

  //Fall back error message

  if ( $errorMessages ) {
    displayForm( $errorMessages, $missingFields, $NewsItem );
  } else {
    $NewsItem->update();
    displaySuccess();
  }
}
//Delete News

function deleteNewsItem() {
  $NewsItem = new NewsItem( array(
    "id" => isset( $_POST["NewsItemId"] ) ? (int) $_POST["NewsItemId"] : "",
  ) );

  $NewsItem->delete();
  displaySuccess();
}

//Display success message when edit was successful

function displaySuccess() {
  $start = isset( $_REQUEST["start"] ) ? (int)$_REQUEST["start"] : 0;
  $order = isset( $_REQUEST["order"] ) ? preg_replace( "/[^ a-zA-Z]/", "", $_REQUEST["order"] ) : "username";
  displayPageHeader( "Changes saved" );
?>
    <p>Your changes have been saved. <a href="edit_NewsItems.php">Return to NewsItem list</a></p>
<?php
  displayPageFooter();
}

?>

