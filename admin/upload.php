<?php
$upload_folder = '../img/'; //The upload folder, might be interesting for the gallery plugin
$filename = pathinfo($_FILES['datei']['name'], PATHINFO_FILENAME);
$extension = strtolower(pathinfo($_FILES['datei']['name'], PATHINFO_EXTENSION));
 
 
//Check for file-extensions
$allowed_extensions = array('png', 'jpg', 'jpeg', 'gif');
if(!in_array($extension, $allowed_extensions)) {
 die("Only png, jpg, jpeg und gifs");
}
 
//Check for filesize
$max_size = 500*1024; //500 KB
if($_FILES['datei']['size'] > $max_size) {
 die("Don't go bigger than 500kb please!");
}
 
//Check for file corruption
if(function_exists('exif_imagetype')) { 
 $allowed_types = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
 $detected_type = exif_imagetype($_FILES['datei']['tmp_name']);
 if(!in_array($detected_type, $allowed_types)) {
 die("Only upload pictures please!");
 }
}
 
//Upload pathing
$new_path = $upload_folder.$filename.'.'.$extension;
 
//Create new filename if this filename already exists
if(file_exists($new_path)) { //put number after the file, if already existing
 $id = 1;
 do {
 $new_path = $upload_folder.$filename.'_'.$id.'.'.$extension;
 $id++;
 } while(file_exists($new_path));
}
 
//All good, move files to new path
move_uploaded_file($_FILES['datei']['tmp_name'], $new_path);
echo 'Picture uploaded!: <a href="'.$new_path.'">'.$new_path.'</a>';
?>