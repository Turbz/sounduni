
<?php
//Include necessary scripts
session_start();
require_once("functions.inc.php");
require_once( "common.inc.php" );
require_once( "config.php" );
require_once( "NewsItem.class.php" );
include ("admincp_head.php");

$start = isset( $_GET["start"] ) ? (int)$_GET["start"] : 0;
$order = isset( $_GET["order"] ) ? preg_replace( "/[^ a-zA-Z]/", "", $_GET["order"] ) : "title";
list( $NewsItems, $totalRows ) = NewsItem::getNewsItems( $start, 6, $order );
?>


<header>
    <div class="container">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
              <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="internal.php">Main</a></li>
                    <li><a href="logout.php">Logout</a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
            </div>
      </nav>
    </div>
</header>
<main>
      <section id="admin_internal">
          <div class="row text-center">
             <h1>All news</h1>
          </div>
      </section>

      <div class="container">
        <section class="row" id="edit_newsitems">
            <div class='col-lg-8 col-md-8 col-sm-12 col-xs-12'>
            <h2> All news list </h2>
              <table cellspacing="0">
                <tr>
                  <th>Title</th>
                  <th>Summary</th>
                  <th>Upload Date</th>
                </tr>
                    <?php
                    $rowCount = 0;

                    foreach ( $NewsItems as $NewsItem ) {
                      $rowCount++;
                    ?>
                <tr<?php if ( $rowCount % 2 == 0 ) echo ' class="alt"' ?>>
                  <td><h4><a href="edit_newsitem.php?NewsItemId=<?php echo $NewsItem->getValueEncoded( "id" ) ?>&amp;start=<?php echo $start ?>&amp;order=<?php echo $order ?>"><?php echo $NewsItem->getValueEncoded( "title" ) ?></a></h4></td>
                  <td><?php echo $NewsItem->getValueEncoded( "summary" ) ?></td>
                  <td><?php echo $NewsItem->getValueEncoded( "uploadDate" ) ?></td>
                </tr>
                    <?php
                    }
                    ?>
              </table>

              <div class="nextprev">
                    <?php if ( $start > 0 ) { ?>
                          <a href="edit_newsitems.php?start=<?php echo max( $start - 6, 0 ) ?>&amp;order=<?php echo $order ?>">Previous page</a>
                    <?php } ?>
                    &nbsp;
                    <?php if ( $start + PAGE_SIZE < $totalRows ) { ?>
                          <a href="edit_newsitems.php?start=<?php echo min( $start + 6, $totalRows ) ?>&amp;order=<?php echo $order ?>">Next page</a>
                    <?php } ?>
              </div>
            </div>
            <div id="edit_upload" class='text-left col-lg-4 col-md-4 col-sm-12 col-xs-12'>
            <h2>Upload news</h2>
              <p>
                        Pellentesque luctus quam quis consequat vulputate. Sed sit amet diam ipsum. Praesent in pellentesque diam, sit amet dignissim erat. Aliquam erat volutpat. Aenean laoreet metus leo, laoreet feugiat enim suscipit quis. Praesent mauris mauris, ornare vitae tincidunt sed, hendrerit eget augue. Nam nec vestibulum nisi, eu dignissim nulla.
                    </p>
              <a href="addnews.php" class="btn btn-light">Add News</a>
            </div>
        </section>
      </div>      
  </main>


