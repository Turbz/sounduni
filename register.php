<?php
session_start();
include ("head.php");
require_once("admin/config.inc.php");
require_once("admin/functions.inc.php");

?>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="img/sounduniverse_logo.svg" alt="Sound Universe" /></a>
                </div>

                <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.php">Go back to the main page</a></li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
    </header>

    <!-- LANDING SECTION -->
    <section id="landing" style="height:40vh">
        <div class="container">
            <div class="row">
                <h1>Register new admin</h1>
                <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <section class="row">

            <div id="edit_form" class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3'>
                <div class="controls">
                    <h1>Register a new admin</h1>
                    <?php
					$showFormular = true; // Make sure to shoe the registrations formular

					if(isset($_GET['register'])) {  // Check for the fields and what was typed in
						$error = false;
						$vorname = trim($_POST['vorname']);
						$nachname = trim($_POST['nachname']);
						$email = trim($_POST['email']);
						$passwort = $_POST['passwort'];
						$passwort2 = $_POST['passwort2'];

						if(empty($vorname) || empty($nachname) || empty($email)) {
							echo 'Please fill out all the fields!<br>';
							$error = true;
						}

						if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
							echo 'Please fill out all the fields!<br>';
							$error = true;
						}
						if(strlen($passwort) == 0) {
							echo 'Please fill out all the fields!<br>';
							$error = true;
						}
						if($passwort != $passwort2) {
							echo 'Passwords do not match<br>';
							$error = true;
						}

						//Check if email address is not already in the database
						if(!$error) {
							$statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
							$result = $statement->execute(array('email' => $email));
							$user = $statement->fetch();

							if($user !== false) {
								echo 'E-mail adress already in use! Please choose a different one.<br>';
								$error = true;
							}
						}

						// No errors, we can register the user now, insert it in
						if(!$error) {
							$passwort_hash = password_hash($passwort, PASSWORD_DEFAULT);

							$statement = $pdo->prepare("INSERT INTO users (email, passwort, vorname, nachname) VALUES (:email, :passwort, :vorname, :nachname)");
							$result = $statement->execute(array('email' => $email, 'passwort' => $passwort_hash, 'vorname' => $vorname, 'nachname' => $nachname));

							if($result) {
								echo 'You successfully added a new admin!. <a href="login.php">Go to Login</a>';
								$showFormular = false;
							} else {
								echo 'Error occured!<br>';
							}
						}
					}

					if($showFormular) {
					?>

                        <form action="?register=1" method="post">
                            <div class="form-group">
                                <label for="inputVorname">Name:</label>
                                <input type="text" id="inputVorname" size="40" maxlength="250" name="vorname" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="inputNachname">Surname:</label>
                                <input type="text" id="inputNachname" size="40" maxlength="250" name="nachname" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail">E-Mail:</label>
                                <input type="email" id="inputEmail" size="40" maxlength="250" name="email" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPasswort">Password:</label>
                                <input type="password" id="inputPasswort" size="40" maxlength="250" name="passwort" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="inputPasswort2">Repeat password:</label>
                                <input type="password" id="inputPasswort2" size="40" maxlength="250" name="passwort2" class="form-control" required>
                            </div>
                            <button type="submit" class="btn btn-dark">Register</button>
                        </form>

                        <?php
					} //Ende von if($showFormular)


					?>
                </div>
            </div>
        </section>
    </div>

    <?php
include ("footer.php");
?>
