<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Purchas Succesfull – Struer Museum</title>
    <meta name="description" content="Purchase tickets for Struer Museum's newest exhibition, The Sound Universe.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <!-- Succes message -->
    <main class="container">
        <section id="success_message" class="text-center row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>Payment Successfull!</h1>
            <p>
                We are so happy for you deciding to visit us at Struer Museum.
            </p>
            <p>We are currently processing your purchase for the museum ticket and you will receive it momentarily.</p>
            <p class="go_back_button"><a href="index.php">Go back to front page</a></p>
        </section>
    </main>

    <!-- Footer without navigation -->
    <footer id="fixed_bottom_footer">
        <div class="container">
            <div class="row">
                <div class="text-center col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h4>General</h4>
                    <p>
                        7600 Struer<br> Sondergade 23<br> mail@struermuseum.dk
                        <br> Tel: +45 9785 1311
                    </p>
                </div>
                <div class="text-center col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h4>Opening hours</h4>
                    <p>
                        Tuesday to Friday<br> 12:00 - 16:00<br> Saturday to Sunday<br> 12:00 - 17:00
                    </p>
                </div>
                <div class="text-center col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <h4>Follow us on</h4>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
