<?php
include ("head.php");
require_once( "admin/common.inc.php" );
require_once( "admin/config.php" );
require_once( "admin/NewsItem.class.php" );


$NewsItemId = isset( $_GET["NewsItemId"] ) ? (int)$_GET["NewsItemId"] : 0;

if ( !$NewsItem = NewsItem::getNewsItem( $NewsItemId ) ) {
  displayPageHeader( "Error" );
  echo "<div>NewsItem not found.</div>";
  displayPageFooter();
  exit;
}
?>

    <body>
        <!-- Preloader -->
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                        <a class="navbar-brand" href="#"><img src="img/sounduniverse_logo.svg" alt="Sound Universe" /></a>
                    </div>

                    <!-- Collect every nav link, forms, and other content and prepare it for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="index.php">Go back to the main page</a></li>

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
            </nav>
        </header>

        <main>
            <!-- LANDING SECTION -->
            <section id="landing" style="height:40vh">
                <div class="container">
                    <div class="row">
                        <h1><?php echo $NewsItem->getValueEncoded( "title" ) ?></h1>
                        <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <section class="row">

                    <div id="news_box" class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3'>
                        <article>
                            <p class="small"> Published on
                                <?php echo $NewsItem->getValueEncoded( "uploadDate" ) ?>
                            </p>
                            <h2><?php echo $NewsItem->getValueEncoded( "title" ) ?></h2>
                            <p>
                                <?php echo $NewsItem->getValueEncoded( "content" ) ?>
                            </p>
                        </article>
                        <div>
                            <a class="btn btn-dark" href="javascript:history.go(-1)">Back</a>
                        </div>
                    </div>


                </section>
            </div>





        </main>

        <?php
 include ("footer.php");
?>
