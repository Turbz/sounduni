// Preloader
$(window).on('load', function() { // makes sure the whole site is loaded
    $('#status').delay(500).fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(350).css({
        'overflow': 'visible'
    });
})


jQuery(window).scroll(function() {
    var fromTopPx = 400; // distance to trigger
    var scrolledFromtop = jQuery(window).scrollTop(); // pixels hidden from view above the scrollable area
    if (scrolledFromtop > fromTopPx) { // the distance from top is bigger than 400
        jQuery('nav').addClass('scrolled');
        jQuery('.navbar-default a').addClass('scrolled');
        jQuery('.navbar-brand').addClass('scrolled');
    } else {
        jQuery('nav').removeClass('scrolled');
        jQuery('.navbar-default a').removeClass('scrolled');
        jQuery('.navbar-brand').removeClass('scrolled');
    }
});

// handle links with @href started with '#' only
$(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');
    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $(id).offset().top - 180;

    // animated top scrolling
    $('body, html').animate({
        scrollTop: pos
    });
});

$(function() {
    //----- OPEN
    $('[data-popup-open]').on('click', function(e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-open'); // save the attribute in a variable
        $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350); // finds data-popup and the targeted number  and mades it fade in

        e.preventDefault(); // prevents adding #  to the URL in the address bar
    });

    //----- CLOSE
    $('[data-popup-close]').on('click', function(e) {
        var targeted_popup_class = jQuery(this).attr('data-popup-close');
        $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});


// Only allowing numbers inputs in ticket formular
$(function() {
    $('#form_number, #form_month, #form_year, #form_ccv').on('keypress', function(ev) {
        var keyCode = window.event ? ev.keyCode : ev.which;
        //codes for 0-9
        if (keyCode < 48 || keyCode > 57) {
            //codes for backspace, delete, enter
            if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
                ev.preventDefault();
            }
        }
    });
});


// Close navbar on a href click - mobile
$(document).on('click', '.navbar-collapse.in', function(e) {
    if ($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
        $(this).collapse('hide');
    }
});
