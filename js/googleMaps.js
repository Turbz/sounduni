var map;
function initMap() {

  // find cordinates of Struer Museum
  var struer_museum = {lat: 56.4899927, lng: 8.5975778};

  // Create new map with location, zoom and styling
  map = new google.maps.Map(document.getElementById('map'), {
    center: struer_museum,
    zoom: 16,
    scrollwheel: false,
    draggable: false,
    disableDefaultUI: true,
    scrollwheel: false,
    zoomControl: false,
    scaleControl: false,
    styles:
    [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
  });


  // Struer Museum marker icon
  var location = new google.maps.Marker({
    position: struer_museum,
    map: map,
    title: 'Struer Museum'
  });

  // InfoWindow content box
  var contentString_museum = '<div id="map_box" class="row">'+
      '<h2 id="map_heading">Struer Museum</h1>'+
      '<div id="map_content">'+
      '<p>7600 Struer, Sondergade 23</p>'+
      '<p>mail@struermuseum.dk</p>'+
      '<p>Tel: +45 9785 1311</p>'+
      '</div>'+
      '</div>';

  // to allow infowindow
  var infowindow_museum = new google.maps.InfoWindow({
    content: contentString_museum
  });

  // Mouseover and Mouseover events
  map.addListener('mouseover', function() {
    infowindow_museum.open(map, location)
  });
  map.addListener('mouseout', function() {
    infowindow_museum.close(map, location)
  });
}
